<?php

namespace App\Repository;

use App\Entity\VoyagesHasVols;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method VoyagesHasVols|null find($id, $lockMode = null, $lockVersion = null)
 * @method VoyagesHasVols|null findOneBy(array $criteria, array $orderBy = null)
 * @method VoyagesHasVols[]    findAll()
 * @method VoyagesHasVols[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoyagesHasVolsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VoyagesHasVols::class);
    }

    // /**
    //  * @return VoyagesHasVols[] Returns an array of VoyagesHasVols objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VoyagesHasVols
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
