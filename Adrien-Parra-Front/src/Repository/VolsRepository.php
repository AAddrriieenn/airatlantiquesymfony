<?php

namespace App\Repository;

use App\Entity\Vols;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\EntityRepository;

/**
 * @method Vols|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vols|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vols[]    findAll()
 * @method Vols[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VolsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Vols::class);
    }

    // /**
    //  * @return Vols[] Returns an array of Vols objects
    //  */
    
    public function findVol($trajet, $vols)
    {
            // ->andWhere('v.exampleField = :val')
            // ->setParameter('val', $value)
            // ->orderBy('v.id', 'ASC')
            // ->setMaxResults(10)
            // ->getQuery()
            // ->getResult()

            $query = $repository->createQueryBuilder('v','t')

            ->join('v.trajet','t')
            ->join('t.vols','v')
            ->orderBy('v.dateDepart','DESC')
            ->getQuery();

            $products = $query->getResult();
        
    }
    

    /*
    public function findOneBySomeField($value): ?Vols
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
