<?php

namespace App\Repository;

use App\Entity\EmployeeHasVols;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EmployeeHasVols|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmployeeHasVols|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmployeeHasVols[]    findAll()
 * @method EmployeeHasVols[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeHasVolsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EmployeeHasVols::class);
    }

    // /**
    //  * @return EmployeeHasVols[] Returns an array of EmployeeHasVols objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EmployeeHasVols
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
