<?php

namespace App\Repository;

use App\Entity\MaintenanceHasEmployee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MaintenanceHasEmployee|null find($id, $lockMode = null, $lockVersion = null)
 * @method MaintenanceHasEmployee|null findOneBy(array $criteria, array $orderBy = null)
 * @method MaintenanceHasEmployee[]    findAll()
 * @method MaintenanceHasEmployee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaintenanceHasEmployeeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MaintenanceHasEmployee::class);
    }

    // /**
    //  * @return MaintenanceHasEmployee[] Returns an array of MaintenanceHasEmployee objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MaintenanceHasEmployee
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
