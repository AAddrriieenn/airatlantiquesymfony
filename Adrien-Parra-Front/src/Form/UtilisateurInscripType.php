<?php

namespace App\Form;

use App\Entity\Clients;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class UtilisateurInscripType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('civilite', ChoiceType::class, [
                'choices'  => [
                'Monsieur' => 'Monsieur',
                'Madame' => 'Madame',
                ],
            ])
            ->add('nom', TextType::class)
            ->add('prenom',  TextType::class)
            ->add('adresse',  TextType::class)
            ->add('ville',  TextType::class)
            ->add('codePostale', TextType::class)
            ->add('dateDeNaissance',  DateType::class)

            ->add('email', EmailType::class,array('label'=> 'Mail'))
            
            ->add('password', RepeatedType::class,array(
                'type' => PasswordType::class,
                'first_options' => array('label' => 'Mot De Passe'),
                'second_options' => array('label'=> 'Confirmer Le Mot De Passe')
                )
            )

            ->add('save', SubmitType::class, array('label'=>"S'Inscrire",
            "attr" => [ "class"=>"waves-effect waves-light btn"]))
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Clients::class,
        ]);
    }
}
