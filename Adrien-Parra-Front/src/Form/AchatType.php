<?php

namespace App\Form;

use App\Entity\Trajet;
use App\Entity\Avion;
use App\Entity\Billets;
use App\Entity\Prix;
use App\Entity\Vols;
use App\Entity\Aeroport;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class AchatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('depart', TextType::class)
            ->add('arrive', TextType::class)
            ->add('dateDepart', DateType::class,[
                'widget' => 'single_text',
                'format' => 'MM-dd-yyyy',
                'attr' => [
                    'class' => 'datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'mm-dd-yyyy'
                ]
            ])
            ->add('nombrePassage', IntegerType::class)
            ->add('save', SubmitType::class, array('label'=>"Reserver le Vol",
            "attr" => [ "class"=>"waves-effect waves-light btn"]));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Avion::class,
        ]);
    }
}
