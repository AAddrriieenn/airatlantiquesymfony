<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190527183958 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE aeroport ADD id_aeroport INT NOT NULL');
        $this->addSql('ALTER TABLE clients DROP pays, CHANGE email email VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE employee ADD id_employee INT NOT NULL');
        $this->addSql('ALTER TABLE trajet ADD id_trajet INT NOT NULL');
        $this->addSql('ALTER TABLE vols ADD id_vols INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE aeroport DROP id_aeroport');
        $this->addSql('ALTER TABLE clients ADD pays VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE email email VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE employee DROP id_employee');
        $this->addSql('ALTER TABLE trajet DROP id_trajet');
        $this->addSql('ALTER TABLE vols DROP id_vols');
    }
}
