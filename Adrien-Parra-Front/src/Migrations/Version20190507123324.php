<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190507123324 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE aeroport (id INT AUTO_INCREMENT NOT NULL, id_aeroport INT NOT NULL, code_aita LONGTEXT NOT NULL, ville VARCHAR(200) NOT NULL, code_postale VARCHAR(50) NOT NULL, pays VARCHAR(200) NOT NULL, nom VARCHAR(200) NOT NULL, code_pays VARCHAR(200) NOT NULL, fuseau_horraire VARCHAR(8) NOT NULL, lat VARCHAR(32) NOT NULL, lon VARCHAR(32) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE avion (id INT AUTO_INCREMENT NOT NULL, id_avion INT NOT NULL, avion VARCHAR(150) NOT NULL, motorisation VARCHAR(150) NOT NULL, passagers INT NOT NULL, capacite_passagers INT NOT NULL, premiere INT NOT NULL, capacite_premiere INT NOT NULL, business INT NOT NULL, capacite_business INT NOT NULL, prenium_eco INT NOT NULL, capacite_prenium_eco INT NOT NULL, type VARCHAR(100) NOT NULL, autonomie INT NOT NULL, capacite_essence INT NOT NULL, vitesse INT NOT NULL, pilote INT NOT NULL, copilote INT NOT NULL, personelle_cabine INT NOT NULL, maintenance INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bagages (id INT AUTO_INCREMENT NOT NULL, id_bagages INT NOT NULL, poids INT NOT NULL, dimension VARCHAR(50) NOT NULL, clients_id_clients INT NOT NULL, billets_id_billets INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE billets (id INT AUTO_INCREMENT NOT NULL, id_billets INT NOT NULL, date DATETIME NOT NULL, porte VARCHAR(10) NOT NULL, numero_de_siege INT NOT NULL, clients_id_clients INT NOT NULL, voyages_id_voyages INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, id_categories INT NOT NULL, libelle VARCHAR(45) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE classes (id INT AUTO_INCREMENT NOT NULL, id_classes INT NOT NULL, libelle VARCHAR(45) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE clients (id INT AUTO_INCREMENT NOT NULL, id_clients INT NOT NULL, civilite VARCHAR(5) NOT NULL, nom VARCHAR(100) NOT NULL, prenom VARCHAR(100) NOT NULL, date_de_naissance DATETIME NOT NULL, telephonne VARCHAR(15) NOT NULL, adresse VARCHAR(100) NOT NULL, code_postale VARCHAR(15) NOT NULL, pays VARCHAR(100) NOT NULL, email VARCHAR(255) NOT NULL, identifiant VARCHAR(100) NOT NULL, mot_de_passe VARCHAR(255) NOT NULL, fidele INT NOT NULL, point_de_fidelite INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee (id INT AUTO_INCREMENT NOT NULL, id_employee INT NOT NULL, civilite VARCHAR(5) NOT NULL, nom VARCHAR(100) NOT NULL, prenom VARCHAR(100) NOT NULL, date_de_naissance DATETIME NOT NULL, telephone VARCHAR(15) NOT NULL, adresse VARCHAR(100) NOT NULL, ville VARCHAR(100) NOT NULL, code_postale VARCHAR(10) NOT NULL, pays VARCHAR(100) NOT NULL, email VARCHAR(255) NOT NULL, identifiant VARCHAR(100) NOT NULL, mot_de_passe VARCHAR(255) NOT NULL, fonction VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee_has_vols (id INT AUTO_INCREMENT NOT NULL, employee_id_employee INT NOT NULL, vols_id_vols INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE incident (id INT AUTO_INCREMENT NOT NULL, id_incident INT NOT NULL, description VARCHAR(255) NOT NULL, commenatire VARCHAR(255) NOT NULL, date DATE NOT NULL, avion_id_avion INT NOT NULL, employee_id_employee INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE maintenance (id INT AUTO_INCREMENT NOT NULL, id_maintenance INT NOT NULL, incident_id_incident INT NOT NULL, incident_avion_id_avion INT NOT NULL, incident_employee_id_employee INT NOT NULL, commentaire VARCHAR(255) NOT NULL, date_debut DATE NOT NULL, date_fin DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE maintenance_has_employee (id INT AUTO_INCREMENT NOT NULL, maintenance_id_maintenance INT NOT NULL, employee_id_employee INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prix (id INT AUTO_INCREMENT NOT NULL, id_prix INT NOT NULL, prix DOUBLE PRECISION NOT NULL, date DATE NOT NULL, classes_id_classes INT NOT NULL, categories_id_categories INT NOT NULL, voyages_id_voyages INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trajet (id INT AUTO_INCREMENT NOT NULL, id_trajet INT NOT NULL, aeroport_depart INT NOT NULL, aeroport_arrive INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vols (id INT AUTO_INCREMENT NOT NULL, id_vols INT NOT NULL, date_depart DATETIME NOT NULL, date_depart_reel DATETIME NOT NULL, date_arrive DATETIME NOT NULL, date_arrive_reel DATETIME NOT NULL, designation VARCHAR(255) NOT NULL, avion_id_avion INT NOT NULL, id_trajet INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voyages (id INT AUTO_INCREMENT NOT NULL, id_voyages INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voyages_has_vols (id INT AUTO_INCREMENT NOT NULL, voyages_has_vols INT NOT NULL, vols_id_vols INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE aeroport');
        $this->addSql('DROP TABLE avion');
        $this->addSql('DROP TABLE bagages');
        $this->addSql('DROP TABLE billets');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE classes');
        $this->addSql('DROP TABLE clients');
        $this->addSql('DROP TABLE employee');
        $this->addSql('DROP TABLE employee_has_vols');
        $this->addSql('DROP TABLE incident');
        $this->addSql('DROP TABLE maintenance');
        $this->addSql('DROP TABLE maintenance_has_employee');
        $this->addSql('DROP TABLE prix');
        $this->addSql('DROP TABLE trajet');
        $this->addSql('DROP TABLE vols');
        $this->addSql('DROP TABLE voyages');
        $this->addSql('DROP TABLE voyages_has_vols');
    }
}
