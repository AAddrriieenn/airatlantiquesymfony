<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;

class ClientConnexionController extends AbstractController
{
    /**
     * @Route("/client/connexion", name="client_connexion")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {


        return $this->render('client_connexion/index.html.twig', [
            'controller_name' => 'ClientConnexionController',
        ]);
    }
}
