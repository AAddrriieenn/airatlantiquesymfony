<?php

namespace App\Controller;

use App\Entity\Clients;
use App\Form\UtilisateurInscripType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ClientInscriptionController extends AbstractController
{
    /**
     * @Route("/client/inscription", name="client_inscription")
     */
    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder, ObjectManager $manager)
    {
              
            $user = new Clients();
            $form = $this->createForm(UtilisateurInscripType::class,$user);
    
            $form->handleRequest($request);
    
            if ($form->isSubmitted() && $form->isValid()) {

                $password = $passwordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
                
                $manager->persist($user);
                $manager->flush();

                return $this->redirectToRoute("client_connexion");
            }
                
            
            return $this->render('client_inscription/index.html.twig', [
                    "form"=>$form->createView()
                ]);
    }
}
