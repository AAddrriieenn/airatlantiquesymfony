<?php

namespace App\Controller;

use App\Entity\Prix;
use App\Entity\Vols;
use App\Entity\Avion;
use App\Entity\Trajet;
use App\Entity\Billets;
use App\Entity\Aeroport;
use App\Form\ReservationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ReservationController extends AbstractController
{
    /**
     * @Route("/reservation", name="reservation")
     */
    public function index(Request $request, ObjectManager $manager, $ville)
    {

    //     $trajets = new Trajet();
    //     $form = $this->createForm(ReservationType::class, $trajets);
        
    
    //     $form->handleRequest($request);
    
    //     if ($form->isSubmitted() && $form->isValid()) {
               
    //         $trajets=$form->getData();

    //         $manager->persist($trajets);
    //         $manager->flush();
    
    //         return $this->redirectToRoute("home");

        $villes = $this->getDoctrine()
            ->getRepository(Aeroport::class)
            ->find($ville);

            if (!$villes) {
                throw $this->createNotFoundException(
                'Aucune ville est trouvé pour ce vol '.$ville
    );
}

    // }
            
            return $this->render('Reservation/index.html.twig', [
                "form"=>$form->createView()
                ]);
    }
}


