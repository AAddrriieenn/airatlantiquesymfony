<?php

namespace App\Controller;

use App\Entity\Avion;
use App\Entity\Billets;
use App\Entity\Prix;
use App\Entity\Vols;
use App\Entity\Clients;
use Symfony\Component\HttpFoundation\Request;
use App\Form\AchatType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AchatBilleController extends AbstractController
{
    /**
     * @Route("/achat/bille", name="achat_bille")
     */
    public function index()
    {
        $repository=$this->getDoctrine()->getRepository(Vols::class);
        $NewBillet = new Vols();

        $id = 0;

        $vol = $repository->find($id);
        $repositoryClient=$this->getDoctrine()->getRepository(Clients::class);
        $client = $repositoryClient->find($idClients);

        
        $NewBillet->getIdClients($client);
        $NewBillet->getIdVols($vol);
        $preniumEco = $vol->getNbPlaceRestanteEco();
        $capacitePremiere = $vol->getNbPlaceRestantePremium();
        $capaciteBusiness = $vol-> getNbPlaceRestanteBusiness();
        if($classe == "economique"){
            $prix = $vol->getPrixEco();
            $vol->setNbPlaceRestanteEco($capacitePreniumEco-1);
        }elseif($classe == "Premium"){
            $prix = $vol->getPrixPremium();
            $vol->setNbPlaceRestantePremium($capacitePremiere-1);
        }elseif($classe == 'business'){
            $prix = $vol->getPrixBusiness();   
            $vol->setNbPlaceRestanteBusiness($capaciteBusiness-1);
        }
        $NewBillet->setNumeroDeSiege($numeroDeSiege);
        $NewBillet->setClasse($classe);
        $NewBillet->setPrix($prix);
        $date = new \DateTime();
        $date->format("Y-m-d \ h-m");
        $NewBillet->setDateReservation($date);

        $em = $this->getDoctrine()->getManager();
        $em->persist($NewBillet);
        $em->flush();

        return $this->render('achat_bille/index.html.twig', [
            'controller_name' => 'AchatBilleController',
        ]);
        
    }
}
