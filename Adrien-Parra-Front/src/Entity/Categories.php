<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoriesRepository")
 */
class Categories
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idCategories;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $libelle;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdCategories(): ?int
    {
        return $this->idCategories;
    }

    public function setIdCategories(int $idCategories): self
    {
        $this->idCategories = $idCategories;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }
}
