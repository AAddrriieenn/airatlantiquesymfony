<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoyagesHasVolsRepository")
 */
class VoyagesHasVols
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $voyages_has_vols;

    /**
     * @ORM\Column(type="integer")
     */
    private $vols_idVols;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVoyagesHasVols(): ?int
    {
        return $this->voyages_has_vols;
    }

    public function setVoyagesHasVols(int $voyages_has_vols): self
    {
        $this->voyages_has_vols = $voyages_has_vols;

        return $this;
    }

    public function getVolsIdVols(): ?int
    {
        return $this->vols_idVols;
    }

    public function setVolsIdVols(int $vols_idVols): self
    {
        $this->vols_idVols = $vols_idVols;

        return $this;
    }
}
