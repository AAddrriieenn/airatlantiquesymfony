<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BilletsRepository")
 */
class Billets
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idBillets;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $porte;

    /**
     * @ORM\Column(type="integer")
     */
    private $numeroDeSiege;

    /**
     * @ORM\Column(type="integer")
     */
    private $Clients_idClients;

    /**
     * @ORM\Column(type="integer")
     */
    private $Voyages_idVoyages;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdBillets(): ?int
    {
        return $this->idBillets;
    }

    public function setIdBillets(int $idBillets): self
    {
        $this->idBillets = $idBillets;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPorte(): ?string
    {
        return $this->porte;
    }

    public function setPorte(string $porte): self
    {
        $this->porte = $porte;

        return $this;
    }

    public function getNumeroDeSiege(): ?int
    {
        return $this->numeroDeSiege;
    }

    public function setNumeroDeSiege(int $numeroDeSiege): self
    {
        $this->numeroDeSiege = $numeroDeSiege;

        return $this;
    }

    public function getClientsIdClients(): ?int
    {
        return $this->Clients_idClients;
    }

    public function setClientsIdClients(int $Clients_idClients): self
    {
        $this->Clients_idClients = $Clients_idClients;

        return $this;
    }

    public function getVoyagesIdVoyages(): ?int
    {
        return $this->Voyages_idVoyages;
    }

    public function setVoyagesIdVoyages(int $Voyages_idVoyages): self
    {
        $this->Voyages_idVoyages = $Voyages_idVoyages;

        return $this;
    }
}
