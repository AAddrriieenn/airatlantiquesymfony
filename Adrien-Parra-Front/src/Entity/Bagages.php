<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BagagesRepository")
 */
class Bagages
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idBagages;

    /**
     * @ORM\Column(type="integer")
     */
    private $poids;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $dimension;

    /**
     * @ORM\Column(type="integer")
     */
    private $Clients_idClients;

    /**
     * @ORM\Column(type="integer")
     */
    private $Billets_idBillets;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdBagages(): ?int
    {
        return $this->idBagages;
    }

    public function setIdBagages(int $idBagages): self
    {
        $this->idBagages = $idBagages;

        return $this;
    }

    public function getPoids(): ?int
    {
        return $this->poids;
    }

    public function setPoids(int $poids): self
    {
        $this->poids = $poids;

        return $this;
    }

    public function getDimension(): ?string
    {
        return $this->dimension;
    }

    public function setDimension(string $dimension): self
    {
        $this->dimension = $dimension;

        return $this;
    }

    public function getClientsIdClients(): ?int
    {
        return $this->Clients_idClients;
    }

    public function setClientsIdClients(int $Clients_idClients): self
    {
        $this->Clients_idClients = $Clients_idClients;

        return $this;
    }

    public function getBilletsIdBillets(): ?int
    {
        return $this->Billets_idBillets;
    }

    public function setBilletsIdBillets(int $Billets_idBillets): self
    {
        $this->Billets_idBillets = $Billets_idBillets;

        return $this;
    }
}
