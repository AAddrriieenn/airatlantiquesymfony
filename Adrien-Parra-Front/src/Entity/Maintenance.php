<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MaintenanceRepository")
 */
class Maintenance
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idMaintenance;

    /**
     * @ORM\Column(type="integer")
     */
    private $Incident_idIncident;

    /**
     * @ORM\Column(type="integer")
     */
    private $Incident_Avion_idAvion;

    /**
     * @ORM\Column(type="integer")
     */
    private $Incident_Employee_idEmployee;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $commentaire;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="date")
     */
    private $dateFin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdMaintenance(): ?int
    {
        return $this->idMaintenance;
    }

    public function setIdMaintenance(int $idMaintenance): self
    {
        $this->idMaintenance = $idMaintenance;

        return $this;
    }

    public function getIncidentIdIncident(): ?int
    {
        return $this->Incident_idIncident;
    }

    public function setIncidentIdIncident(int $Incident_idIncident): self
    {
        $this->Incident_idIncident = $Incident_idIncident;

        return $this;
    }

    public function getIncidentAvionIdAvion(): ?int
    {
        return $this->Incident_Avion_idAvion;
    }

    public function setIncidentAvionIdAvion(int $Incident_Avion_idAvion): self
    {
        $this->Incident_Avion_idAvion = $Incident_Avion_idAvion;

        return $this;
    }

    public function getIncidentEmployeeIdEmployee(): ?int
    {
        return $this->Incident_Employee_idEmployee;
    }

    public function setIncidentEmployeeIdEmployee(int $Incident_Employee_idEmployee): self
    {
        $this->Incident_Employee_idEmployee = $Incident_Employee_idEmployee;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }
}
