<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClassesRepository")
 */
class Classes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idClasses;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $libelle;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdClasses(): ?int
    {
        return $this->idClasses;
    }

    public function setIdClasses(int $idClasses): self
    {
        $this->idClasses = $idClasses;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }
}
