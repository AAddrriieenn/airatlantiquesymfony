<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvionRepository")
 */
class Avion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idAvion;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $avion;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $motorisation;

    /**
     * @ORM\Column(type="integer")
     */
    private $passagers;

    /**
     * @ORM\Column(type="integer")
     */
    private $capacitePassagers;

    /**
     * @ORM\Column(type="integer")
     */
    private $premiere;

    /**
     * @ORM\Column(type="integer")
     */
    private $capacitePremiere;

    /**
     * @ORM\Column(type="integer")
     */
    private $business;

    /**
     * @ORM\Column(type="integer")
     */
    private $capaciteBusiness;

    /**
     * @ORM\Column(type="integer")
     */
    private $preniumEco;

    /**
     * @ORM\Column(type="integer")
     */
    private $capacitePreniumEco;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $autonomie;

    /**
     * @ORM\Column(type="integer")
     */
    private $capaciteEssence;

    /**
     * @ORM\Column(type="integer")
     */
    private $vitesse;

    /**
     * @ORM\Column(type="integer")
     */
    private $pilote;

    /**
     * @ORM\Column(type="integer")
     */
    private $copilote;

    /**
     * @ORM\Column(type="integer")
     */
    private $personelleCabine;

    /**
     * @ORM\Column(type="integer")
     */
    private $maintenance;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdAvion(): ?int
    {
        return $this->idAvion;
    }

    public function setIdAvion(int $idAvion): self
    {
        $this->idAvion = $idAvion;

        return $this;
    }

    public function getAvion(): ?string
    {
        return $this->avion;
    }

    public function setAvion(string $avion): self
    {
        $this->avion = $avion;

        return $this;
    }

    public function getMotorisation(): ?string
    {
        return $this->motorisation;
    }

    public function setMotorisation(string $motorisation): self
    {
        $this->motorisation = $motorisation;

        return $this;
    }

    public function getPassagers(): ?int
    {
        return $this->passagers;
    }

    public function setPassagers(int $passagers): self
    {
        $this->passagers = $passagers;

        return $this;
    }

    public function getCapacitePassagers(): ?int
    {
        return $this->capacitePassagers;
    }

    public function setCapacitePassagers(int $capacitePassagers): self
    {
        $this->capacitePassagers = $capacitePassagers;

        return $this;
    }

    public function getPremiere(): ?int
    {
        return $this->premiere;
    }

    public function setPremiere(int $premiere): self
    {
        $this->premiere = $premiere;

        return $this;
    }

    public function getCapacitePremiere(): ?int
    {
        return $this->capacitePremiere;
    }

    public function setCapacitePremiere(int $capacitePremiere): self
    {
        $this->capacitePremiere = $capacitePremiere;

        return $this;
    }

    public function getBusiness(): ?int
    {
        return $this->business;
    }

    public function setBusiness(int $business): self
    {
        $this->business = $business;

        return $this;
    }

    public function getCapaciteBusiness(): ?int
    {
        return $this->capaciteBusiness;
    }

    public function setCapaciteBusiness(int $capaciteBusiness): self
    {
        $this->capaciteBusiness = $capaciteBusiness;

        return $this;
    }

    public function getPreniumEco(): ?int
    {
        return $this->preniumEco;
    }

    public function setPreniumEco(int $preniumEco): self
    {
        $this->preniumEco = $preniumEco;

        return $this;
    }

    public function getCapacitePreniumEco(): ?int
    {
        return $this->capacitePreniumEco;
    }

    public function setCapacitePreniumEco(int $capacitePreniumEco): self
    {
        $this->capacitePreniumEco = $capacitePreniumEco;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAutonomie(): ?int
    {
        return $this->autonomie;
    }

    public function setAutonomie(int $autonomie): self
    {
        $this->autonomie = $autonomie;

        return $this;
    }

    public function getCapaciteEssence(): ?int
    {
        return $this->capaciteEssence;
    }

    public function setCapaciteEssence(int $capaciteEssence): self
    {
        $this->capaciteEssence = $capaciteEssence;

        return $this;
    }

    public function getVitesse(): ?int
    {
        return $this->vitesse;
    }

    public function setVitesse(int $vitesse): self
    {
        $this->vitesse = $vitesse;

        return $this;
    }

    public function getPilote(): ?int
    {
        return $this->pilote;
    }

    public function setPilote(int $pilote): self
    {
        $this->pilote = $pilote;

        return $this;
    }

    public function getCopilote(): ?int
    {
        return $this->copilote;
    }

    public function setCopilote(int $copilote): self
    {
        $this->copilote = $copilote;

        return $this;
    }

    public function getPersonelleCabine(): ?int
    {
        return $this->personelleCabine;
    }

    public function setPersonelleCabine(int $personelleCabine): self
    {
        $this->personelleCabine = $personelleCabine;

        return $this;
    }

    public function getMaintenance(): ?int
    {
        return $this->maintenance;
    }

    public function setMaintenance(int $maintenance): self
    {
        $this->maintenance = $maintenance;

        return $this;
    }
}
