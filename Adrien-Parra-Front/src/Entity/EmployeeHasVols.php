<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeHasVolsRepository")
 */
class EmployeeHasVols
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Employee_idEmployee;

    /**
     * @ORM\Column(type="integer")
     */
    private $Vols_idVols;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmployeeIdEmployee(): ?int
    {
        return $this->Employee_idEmployee;
    }

    public function setEmployeeIdEmployee(int $Employee_idEmployee): self
    {
        $this->Employee_idEmployee = $Employee_idEmployee;

        return $this;
    }

    public function getVolsIdVols(): ?int
    {
        return $this->Vols_idVols;
    }

    public function setVolsIdVols(int $Vols_idVols): self
    {
        $this->Vols_idVols = $Vols_idVols;

        return $this;
    }
}
