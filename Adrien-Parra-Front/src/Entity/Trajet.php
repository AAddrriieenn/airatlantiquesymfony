<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TrajetRepository")
 */
class Trajet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idTrajet;

    /**
     * @ORM\Column(type="integer")
     */
    private $aeroportDepart;

    /**
     * @ORM\Column(type="integer")
     */
    private $aeroportArrive;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Vols", mappedBy="trajet")
     */
    private $vol;


    public function __construct()
    {
        $this->vol = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdTrajet(): ?int
    {
        return $this->idTrajet;
    }

    public function setIdTrajet(int $idTrajet): self
    {
        $this->idTrajet = $idTrajet;

        return $this;
    }

    public function getAeroportDepart(): ?int
    {
        return $this->aeroportDepart;
    }

    public function setAeroportDepart(int $aeroportDepart): self
    {
        $this->aeroportDepart = $aeroportDepart;

        return $this;
    }

    public function getAeroportArrive(): ?int
    {
        return $this->aeroportArrive;
    }

    public function setAeroportArrive(int $aeroportArrive): self
    {
        $this->aeroportArrive = $aeroportArrive;

        return $this;
    }

    /**
     * @return Collection|Vols[]
     */
    public function getVol(): Collection
    {
        return $this->vol;
    }
}
