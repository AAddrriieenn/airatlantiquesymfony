<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PrixRepository")
 */
class Prix
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idPrix;

    /**
     * @ORM\Column(type="float")
     */
    private $prix;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $Classes_idClasses;

    /**
     * @ORM\Column(type="integer")
     */
    private $Categories_idCategories;

    /**
     * @ORM\Column(type="integer")
     */
    private $Voyages_idVoyages;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdPrix(): ?int
    {
        return $this->idPrix;
    }

    public function setIdPrix(int $idPrix): self
    {
        $this->idPrix = $idPrix;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getClassesIdClasses(): ?int
    {
        return $this->Classes_idClasses;
    }

    public function setClassesIdClasses(int $Classes_idClasses): self
    {
        $this->Classes_idClasses = $Classes_idClasses;

        return $this;
    }

    public function getCategoriesIdCategories(): ?int
    {
        return $this->Categories_idCategories;
    }

    public function setCategoriesIdCategories(int $Categories_idCategories): self
    {
        $this->Categories_idCategories = $Categories_idCategories;

        return $this;
    }

    public function getVoyagesIdVoyages(): ?int
    {
        return $this->Voyages_idVoyages;
    }

    public function setVoyagesIdVoyages(int $Voyages_idVoyages): self
    {
        $this->Voyages_idVoyages = $Voyages_idVoyages;

        return $this;
    }
}
