<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoyagesRepository")
 */
class Voyages
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idVoyages;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdVoyages(): ?int
    {
        return $this->idVoyages;
    }

    public function setIdVoyages(int $idVoyages): self
    {
        $this->idVoyages = $idVoyages;

        return $this;
    }
}
