<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IncidentRepository")
 */
class Incident
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idIncident;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $commenatire;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $Avion_idAvion;

    /**
     * @ORM\Column(type="integer")
     */
    private $Employee_idEmployee;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdIncident(): ?int
    {
        return $this->idIncident;
    }

    public function setIdIncident(int $idIncident): self
    {
        $this->idIncident = $idIncident;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCommenatire(): ?string
    {
        return $this->commenatire;
    }

    public function setCommenatire(string $commenatire): self
    {
        $this->commenatire = $commenatire;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getAvionIdAvion(): ?int
    {
        return $this->Avion_idAvion;
    }

    public function setAvionIdAvion(int $Avion_idAvion): self
    {
        $this->Avion_idAvion = $Avion_idAvion;

        return $this;
    }

    public function getEmployeeIdEmployee(): ?int
    {
        return $this->Employee_idEmployee;
    }

    public function setEmployeeIdEmployee(int $Employee_idEmployee): self
    {
        $this->Employee_idEmployee = $Employee_idEmployee;

        return $this;
    }
}
