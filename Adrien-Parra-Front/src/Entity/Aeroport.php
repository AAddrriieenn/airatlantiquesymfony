<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AeroportRepository")
 */
class Aeroport
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idAeroport;

    /**
     * @ORM\Column(type="text")
     */
    private $codeAITA;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $codePostale;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $pays;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $codePays;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $fuseauHorraire;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $lat;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $lon;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdAeroport(): ?int
    {
        return $this->idAeroport;
    }

    public function setIdAeroport(int $idAeroport): self
    {
        $this->idAeroport = $idAeroport;

        return $this;
    }

    public function getCodeAITA(): ?string
    {
        return $this->codeAITA;
    }

    public function setCodeAITA(string $codeAITA): self
    {
        $this->codeAITA = $codeAITA;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCodePostale(): ?string
    {
        return $this->codePostale;
    }

    public function setCodePostale(string $codePostale): self
    {
        $this->codePostale = $codePostale;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCodePays(): ?string
    {
        return $this->codePays;
    }

    public function setCodePays(string $codePays): self
    {
        $this->codePays = $codePays;

        return $this;
    }

    public function getFuseauHorraire(): ?string
    {
        return $this->fuseauHorraire;
    }

    public function setFuseauHorraire(string $fuseauHorraire): self
    {
        $this->fuseauHorraire = $fuseauHorraire;

        return $this;
    }

    public function getLat(): ?string
    {
        return $this->lat;
    }

    public function setLat(string $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLon(): ?string
    {
        return $this->lon;
    }

    public function setLon(string $lon): self
    {
        $this->lon = $lon;

        return $this;
    }
}
