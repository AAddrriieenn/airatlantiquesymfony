<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MaintenanceHasEmployeeRepository")
 */
class MaintenanceHasEmployee
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Maintenance_idMaintenance;

    /**
     * @ORM\Column(type="integer")
     */
    private $Employee_idEmployee;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMaintenanceIdMaintenance(): ?int
    {
        return $this->Maintenance_idMaintenance;
    }

    public function setMaintenanceIdMaintenance(int $Maintenance_idMaintenance): self
    {
        $this->Maintenance_idMaintenance = $Maintenance_idMaintenance;

        return $this;
    }

    public function getEmployeeIdEmployee(): ?int
    {
        return $this->Employee_idEmployee;
    }

    public function setEmployeeIdEmployee(int $Employee_idEmployee): self
    {
        $this->Employee_idEmployee = $Employee_idEmployee;

        return $this;
    }
}
