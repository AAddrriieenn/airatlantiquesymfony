<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VolsRepository")
 */
class Vols
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idVols;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateDepart;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateDepartReel;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateArrive;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateArriveReel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $designation;

    /**
     * @ORM\Column(type="integer")
     */
    private $Avion_idAvion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Trajet", inversedBy="vol")
     */
    private $trajet;

    public function __construct()
    {
        $this->trajets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdVols(): ?int
    {
        return $this->idVols;
    }

    public function setIdVols(int $idVols): self
    {
        $this->idVols = $idVols;

        return $this;
    }

    public function getDateDepart(): ?\DateTimeInterface
    {
        return $this->dateDepart;
    }

    public function setDateDepart(\DateTimeInterface $dateDepart): self
    {
        $this->dateDepart = $dateDepart;

        return $this;
    }

    public function getDateDepartReel(): ?\DateTimeInterface
    {
        return $this->dateDepartReel;
    }

    public function setDateDepartReel(\DateTimeInterface $dateDepartReel): self
    {
        $this->dateDepartReel = $dateDepartReel;

        return $this;
    }

    public function getDateArrive(): ?\DateTimeInterface
    {
        return $this->dateArrive;
    }

    public function setDateArrive(\DateTimeInterface $dateArrive): self
    {
        $this->dateArrive = $dateArrive;

        return $this;
    }

    public function getDateArriveReel(): ?\DateTimeInterface
    {
        return $this->dateArriveReel;
    }

    public function setDateArriveReel(\DateTimeInterface $dateArriveReel): self
    {
        $this->dateArriveReel = $dateArriveReel;

        return $this;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getAvionIdAvion(): ?int
    {
        return $this->Avion_idAvion;
    }

    public function setAvionIdAvion(int $Avion_idAvion): self
    {
        $this->Avion_idAvion = $Avion_idAvion;

        return $this;
    }

    public function getIdTrajet(): ?int
    {
        return $this->idTrajet;
    }

    public function setIdTrajet(int $idTrajet): self
    {
        $this->idTrajet = $idTrajet;

        return $this;
    }

    public function getTrajet(): ?Trajet
    {
        return $this->trajet;
    }

    public function setTrajet(?Trajet $trajet): self
    {
        $this->trajet = $trajet;

        return $this;
    }

    /**
     * @return Collection|Trajet[]
     */
    public function getTrajets(): Collection
    {
        return $this->trajets;
    }

    public function addTrajet(Trajet $trajet): self
    {
        if (!$this->trajets->contains($trajet)) {
            $this->trajets[] = $trajet;
            $trajet->setTrajet($this);
        }

        return $this;
    }

    public function removeTrajet(Trajet $trajet): self
    {
        if ($this->trajets->contains($trajet)) {
            $this->trajets->removeElement($trajet);
            // set the owning side to null (unless already changed)
            if ($trajet->getTrajet() === $this) {
                $trajet->setTrajet(null);
            }
        }

        return $this;
    }
}
